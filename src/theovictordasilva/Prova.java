package theovictordasilva;
public class Prova {
    //Patter Strategy !!
    public static void main(String[] args) {
        //1000 positions vector
        SuperArray array = new SuperArray(1000);
        for (int i = 0; i < array.getLenght(); i++) {
            array.setValue(i, Math.random()*1000);
        }
        long Time = array.Sort(new BubbleSort(), SortOrder.DESCENDING);
        System.out.println("Array ordenado em bubbleSort Ascending");
        System.out.println(array.toString());
        System.out.println("Sort in:"+Time+" miliseconds");
        
        Time = array.Sort(new SelectionSort(), SortOrder.DESCENDING);
        System.out.println("Array ordenado em selectionSort Ascending");
        System.out.println(array.toString());
        System.out.println("Sort in:"+Time+" miliseconds");
        
    }
}
